package com.SimpleBoard.Mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.SimpleBoard.Model.LoginDto;

@Repository
@Mapper
public interface LoginMapper {
	LoginDto getLoginInfo(LoginDto login) throws Exception;
	void registerUser(LoginDto dto) throws Exception;
	List<LoginDto> getUserList() throws Exception;
}