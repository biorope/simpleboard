package com.SimpleBoard.Mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import com.SimpleBoard.Model.CreateDto;

@Repository
@Mapper
public interface CreateMapper {
	List<CreateDto> getCreateInfo() throws Exception;
}
