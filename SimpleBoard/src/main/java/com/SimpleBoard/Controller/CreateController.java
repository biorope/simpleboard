package com.SimpleBoard.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;


import com.SimpleBoard.Model.CreateDto;
import com.SimpleBoard.Service.CreateService;

@Controller
public class CreateController {

	@Autowired
	CreateService createservice;
	
	@GetMapping(value = "/create")
	public String getCreate() throws Exception{
		return "create";
	}
	
	@PostMapping(value = "/create")
	public String setCreateInfo(CreateDto create, Model model) throws Exception{
		List<CreateDto> resultList = createservice.getCreateInfo(); 

//		CreateDto result = 
// 		if(resultList.length()) {
		return "redirect:/board";
//		} else {
//			model.addAttribute("fail", "Create failed check all box");
//			return "create";
//		}
	}
	
	@GetMapping(value = "/board")
	public String table(Model model) throws Exception {
		List<CreateDto> resultList = createservice.getCreateInfo();
		model.addAttribute("List", resultList);
		
		return "table";
	}

	
}
