package com.SimpleBoard.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.SimpleBoard.Model.LoginDto;
import com.SimpleBoard.Service.LoginService;


@Controller
public class LoginController {
	@Autowired
	LoginService loginService;

	@GetMapping(value = "/")
	public String Main() {
		return "home";
	}

	@PostMapping(value = "/login")
	public String getLoginInfo(LoginDto login, Model model) throws Exception {
		LoginDto result = loginService.getLoginInfo(login);
		if (result != null) {

			model.addAttribute("result", result.getSimpleId() + " Welcome!!!");

			System.out.println("test");

			return "redirect:/board";
			
		} else {
			model.addAttribute("fail", "Login failed check ID, PW");
			return "table";
		}
	}

	@GetMapping(value = "/userregister")
	public String getRegister() throws Exception {
		return "userRegister";
	}

	@PostMapping(value = "/userregister")
	public String postRegister(LoginDto dto) throws Exception {
		loginService.registerUser(dto);

		return "home";
	}

	@GetMapping(value = "/findidpw")
	public String findIdPassword() throws Exception {
		return "findIdPassword";
	}

	@GetMapping(value = "/userlist")
	public String getUserList(Model model) throws Exception {
		model.addAttribute("lists", loginService.getUserList());
		return "userlist";
	}

	@GetMapping(value = "/companypage")
	public String companypage() throws Exception {
		return "companypage";
	}

}
