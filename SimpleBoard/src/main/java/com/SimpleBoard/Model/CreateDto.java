package com.SimpleBoard.Model;

public class CreateDto {

	private String Company;
	private String Ticker;
	private String Category;
	private String IPODate;
	private String IPOVolum;
	private String CompanyLocation;
	private String CompanyDescription;
	private String IndustryAnalysis;
	private String Link1;
	private String Link2;
	private String Link3;
	
	public String getCompany() {
		return Company;
	}
	public void setCompany(String Company) {
		this.Company = Company;
	}
	public String getTicker() {
		return Ticker;
	}
	public void setTicker(String Ticker) {
		this.Ticker = Ticker;
	}
	public String getCategory() {
		return Category;
	}
	public void setCategory(String Category) {
		this.Category = Category;
	}
	public String getIPODate() {
		return IPODate;
	}
	public void setIPODate(String IPODate) {
		this.IPODate = IPODate;
	}
	public String getIPOVolum() {
		return IPOVolum;
	}
	public void setIPOVolum(String IPOVolum) {
		this.IPOVolum = IPOVolum;
	}
	public String getCompanyLocation() {
		return CompanyLocation;
	}
	public void setCompanyLocation(String CompanyLocation) {
		this.CompanyLocation = CompanyLocation;
	}
	public String getCompanyDescription() {
		return CompanyDescription;
	}
	public void setCompanyDescription(String CompanyDescription) {
		this.CompanyDescription = CompanyDescription;
	}
	public String getIndustryAnalysis() {
		return IndustryAnalysis;
	}
	public void setIndustryAnalysis(String IndustryAnalysis) {
		this.IndustryAnalysis = IndustryAnalysis;
	}
	public String getLink1() {
		return Link1;
	}
	public void setLink1(String Link1) {
		this.Link1 = Link1;
	}
	public String getLink2() {
		return Link2;
	}
	public void setLink2(String Link2) {
		this.Link2 = Link2;
	}
	public String getLink3() {
		return Link3;
	}
	public void setLink3(String Link3) {
		this.Link3 = Link3;
	}
	
}
