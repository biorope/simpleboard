package com.SimpleBoard.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.SimpleBoard.Mapper.CreateMapper;
import com.SimpleBoard.Model.CreateDto;

@Service
public class CreateServicempl implements CreateService {
	@Autowired
	private CreateMapper createMapper;
	
	public List<CreateDto> getCreateInfo() throws Exception{
		return createMapper.getCreateInfo();
	}
}
