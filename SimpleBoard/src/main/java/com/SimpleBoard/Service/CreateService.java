package com.SimpleBoard.Service;

import java.util.List;

import com.SimpleBoard.Model.CreateDto;

public interface CreateService {

	List<CreateDto> getCreateInfo() throws Exception;
	
	
}
