package com.SimpleBoard.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.SimpleBoard.Mapper.LoginMapper;
import com.SimpleBoard.Model.LoginDto;

@Service
public class LoginServiceImpl implements LoginService {
	@Autowired
	private LoginMapper loginMapper;
	
	public LoginDto getLoginInfo(LoginDto login) throws Exception {
		return loginMapper.getLoginInfo(login);
	}

	@Override
	public void registerUser(LoginDto login) throws Exception {
		loginMapper.registerUser(login);		
	}
	
	
	@Override
	public List<LoginDto> getUserList() throws Exception {
		return loginMapper.getUserList();
	}
	
}