package com.SimpleBoard.Service;

import java.util.List;

import com.SimpleBoard.Model.LoginDto;

public interface LoginService {
	LoginDto getLoginInfo(LoginDto login) throws Exception;
	
	void registerUser(LoginDto login) throws Exception;
	List<LoginDto> getUserList() throws Exception;
}

